import torch as th
import util
import mean_shift as ms
import matplotlib.pyplot as plt
import os
import k_means as km


base_path = 'kdes'
h = 0.05
# Number of discretization points along one dimension in DCT space
n_points = 50
n_c = 5000
h_km = 0.04
batchsize = 100


def calc_kde():
    device = th.device('cpu')
    p = 2
    dct_dims = p ** 2 - 1
    gx, gy, gz = th.meshgrid([th.linspace(-1, 1, n_points) for _ in range(3)])
    gx, gy, gz = gx.to(device), gy.to(device), gz.to(device)
    dct_all = util.get_train_data(p).to(device)
    grid = th.stack((gx, gy, gz), dim=-1).view(
        -1, dct_dims
    ).to(device).to(th.float64)
    kdes = []
    # adjust the 500 such that the data still fits on your gpu
    print(grid.shape, dct_all.shape)
    done_ = 0
    all_ = grid.shape[0] 
    for points in th.split(grid, batchsize):
        kdes.append(ms.kde(points, dct_all, h))
        print("kde:", done_, "/",  all_)
        #print("kde:", done_, "/",  all_, "kdes", kdes)
        done_ = done_ + batchsize
    kdes = th.stack(kdes).view(n_points, n_points, n_points)
    th.save(kdes, os.path.join(base_path, f'true_h_{h}.pth'))

def calc_kde_km():
    device = th.device('cpu')
    p = 2
    dct_dims = p ** 2 - 1
    gx, gy, gz = th.meshgrid([th.linspace(-1, 1, n_points) for _ in range(3)])
    gx, gy, gz = gx.to(device), gy.to(device), gz.to(device)
    dct_bank, bank_weights = km.get_data(500, p, device=device)
    grid = th.stack((gx, gy, gz), dim=-1).view(
        -1, dct_dims
    ).to(device).to(th.float64)
    kdes = []
    # adjust the 500 such that the data still fits on your gpu
    print(grid.shape, dct_bank.shape)
    done_ = 0
    all_ = grid.shape[0] 
    for points in th.split(grid, batchsize):
        kdes.append(ms.kde(points, dct_bank, h_km, w=bank_weights))
        print("kde:", done_, "/",  all_)
        #print("kde:", done_, "/",  all_, "kdes", kdes)
        done_ = done_ + batchsize
    kdes = th.stack(kdes).view(n_points, n_points, n_points)
    th.save(kdes, os.path.join(base_path, f'true_h_{h_km}_km.pth'))


def visualize(mode):
    path_true = os.path.join(base_path, f'true_h_{h}_{mode}.pth')

    # Marginalization over the three dimensions
    kde_load = th.load(path_true, map_location='cpu')
    print(kde_load.shape)
    kde_clean_0 = th.clamp_min(th.log(th.sum(kde_load, dim=0)).cpu(), -30) # marg over dim 0
    kde_clean_1 = th.clamp_min(th.log(th.sum(kde_load, dim=1)).cpu(), -30)
    kde_clean_2 = th.clamp_min(th.log(th.sum(kde_load, dim=2)).cpu(), -30)

    gx, gy = th.meshgrid([th.linspace(-1, 1, kde_clean_0.shape[0]) for _ in range(2)])

    f = plt.figure()
    a = f.add_subplot(1, 1, 1, projection='3d')
    a.plot_surface(gx.numpy(), gy.numpy(), kde_clean_0.numpy())
    a.set_zlim(-31, 1)

    f = plt.figure()
    a = f.add_subplot(1, 1, 1, projection='3d')
    a.plot_surface(gx.numpy(), gy.numpy(), kde_clean_1.numpy())
    a.set_zlim(-31, 1)

    f = plt.figure()
    a = f.add_subplot(1, 1, 1, projection='3d')
    a.plot_surface(gx.numpy(), gy.numpy(), kde_clean_2.numpy())
    a.set_zlim(-31, 1)

    # f, a = plt.subplots(1, 1)
    # a.imshow(kde_clean.numpy(), vmin=-20, vmax=1.1)
    plt.show()

def visualize_mean_shift(pnsr, estimates, h_, nr_traj, show_dim=[0,1], marg_dim=2):
    
    # path_true = os.path.join(base_path, f'true_h_{h}_ref.pth')
    path_true = os.path.join(base_path, f'true_h_{h}_km.pth')
     # Marginalization over the three dimensions
    kde_load = th.load(path_true, map_location='cpu')
    kde_clean = th.clamp_min(th.log(th.sum(kde_load, dim=marg_dim)).cpu(), -30) # marg over dim 0

    nr_updates = len(pnsr)
    traj_estimates = [[estimates[i][sample][show_dim].numpy() for i in range(nr_updates)] for sample in range(nr_traj)]

    # print(kde_clean.shape)
    ticks = th.linspace(-1, 1, 50).numpy()

    f, a = plt.subplots(1, 1)
    # a.imshow(kde_clean.numpy(), vmin=-20, vmax=1.1)
    a.pcolor(ticks, ticks, kde_clean.numpy(), vmin=-20, vmax=1.1)
    for traj_estimate in traj_estimates:
        # print(traj_estimate)
        x, y = zip(*traj_estimate)
        #plt.plot(traj_estimate)
        # plt.show()
        a.plot(x[0], y[0], marker='o', color='black')
        a.plot(x, y)
    plt.show()

    plt.plot(pnsr)
    plt.xlabel("Iteration")
    plt.ylabel("PSNR")
    plt.show()

if __name__ == '__main__':
    # print("reference:")
    # visualize("ref", 0) # amrginalization over dim 0
    # visualize("ref", 1) # marg over dim 1
    # visualize("ref", 2) # marg over dim 2
    
    #calc_kde_km()
    #print("km approx with h_km =", h_km)
    visualize("ref")
