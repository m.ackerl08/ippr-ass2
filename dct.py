import torch as th
import math

import numpy as np # test
import scipy.fftpack as sc_fft # test

def mat1d(n):

    #n_withflipped = torch.cat(n, torch.flip(n, 0))
    #N = th.fft.rfft(n_withflipped)
    #k = th.linspace(0, N-1, N)
 
    #return th.real(th.exp(-j * math.pi / (2 * N) * k) * N[0:N])
    pass

def mat2d(n):
    '''
    Implement the 2D DCT matrix as the kronecker product of two 1D DCT matrices
    '''

    # not allowed !!:
    # c_test = sc_fft.dct(np.eye(n), axis=0, norm='ortho')
    # C_test = np.kron(c_test,c_test)
    # return th.tensor(C_test)

    C_in = th.eye(n)
    C = th.zeros_like(C_in)

    for k1 in range(n):
        for k0 in range(n):
            for n0 in range (n):
                C[k0,k1] = C[k0,k1] + 2* C_in[n0, k1] * math.cos(math.pi*k0*(2*n0 + 1) / (2*n))
            if k0 == 0:
                C[k0, k1] =C[k0, k1]*math.sqrt(1/(4*n))
            else:
                C[k0, k1] =C[k0, k1]*math.sqrt(1/(2*n))

    C_2D = th.zeros([n**2, n**2], dtype=th.double)
    for n1 in range(n):
        for n2 in range(n):
            for k1 in range(n):
                for k2 in range(n):
                    C_2D[n1*n + k1, n2*n + k2] = C[n1, n2] * C[k1, k2]
    return C_2D


if __name__ == '__main__':
    th.set_printoptions(4, sci_mode=False)
    n = th.tensor(2)
    C = mat2d(n)
    # This should be the (n^2 \times n^2) identity matrix

    c_test = sc_fft.dct(np.eye(n), axis=0, norm='ortho')
    C_test = np.kron(c_test,c_test)
    assert np.allclose(C.numpy(), C_test)

    print(C)
    print((C.T @ C))
