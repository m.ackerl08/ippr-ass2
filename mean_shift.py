import torch as th
import imageio
import matplotlib.pyplot as plt
import dct
import k_means as km
import math
import util
import vis


def mean_shift(estimate, noisy, dct_bank, bank_weights, sigma, h, D, prefactor):
    '''
    dct_bank = c
    Implement the modified mean shift algorithm (Eq. 21)
    '''
    #print("------------------------- MEANSHIFT")
    Dx = D @ estimate.T
    bank_weights = bank_weights.type(th.DoubleTensor)
    N_est = estimate.shape[0]

    # print("D", D.shape, "Dx", Dx.shape)
    # print("prefactor", prefactor.shape)
    # print("dct_bank", dct_bank.shape, dct_bank.type())
    # print("bank_weights", bank_weights.shape, bank_weights.type())
    # print("estimate", estimate.shape)
    # print("noisy", noisy.shape)
    # print("")

    #old

    # sum1 = th.sum(
    #             dct_bank.T * 
    #             bank_weights * 
    #             th.exp(
    #                 -1.0/(2*h**2) * 
    #                 th.sum(th.pow(th.cdist(dct_bank, Dx.T), 2.0), dim=1) / N_est 
    #             ),
    #         dim=1)
    # # print("sum1", sum1.shape)

    # sum2 = th.sum( 
    #             bank_weights * 
    #             th.exp(
    #                 -1.0/(2*h**2) * 
    #                 th.sum(th.pow(th.cdist(dct_bank, Dx.T), 2.0), dim=1) / N_est 
    #             ))
    # # print("sum2", sum2)
    # result = prefactor @ ((1/h**2) * (D.T @ (sum1 / sum2)).T + noisy / (sigma**2)).T
    
    #new

    # tmp_w_tilde = bank_weights * th.exp(-1/(2* h**2) * th.pow(th.cdist(dct_bank, Dx.T), 2)).T
    tmp_w_tilde = bank_weights * th.exp(-1.0/(2* h**2) * th.pow(th.cdist(dct_bank, Dx.T), 2)).T
    # print("tmp_w_tilde", tmp_w_tilde.shape)

    sum1 = th.sum(th.einsum('bi,bj->bij', dct_bank, tmp_w_tilde.T), dim=0)
    sum2 = th.sum(tmp_w_tilde, dim=1)
    result = prefactor @ ((1.0/(h**2)) * (D.T @ (sum1 / sum2)).T + noisy / (sigma**2)).T
    #print("sum1", sum1.shape, "sum2", sum2.shape)
    #print("result", result.shape)
    



    #print("result", result.shape)
    return result.T


def kde(x, c, h, w=None):
    '''
    Implement the KDE of the prior (Eq. 7)
    '''
    #print("KDE")
    #print("shape: (x, c) ", x.shape, c.shape)
    #print("w", w.shape)
    d = x.shape[1]
    N_batch = x.shape[0]
    N = c.shape[0]
    #print("d, N_batch, N:", d, N_batch, N)
    if w is None:
        tmp =1 / ((2*math.pi)**(d/2) * N * h**d) * th.sum(th.exp(-1/(2* h**2) * th.pow(th.cdist(c, x), 2)), dim=0)
    else:
        tmp =1 / ((2*math.pi)**(d/2) * N * h**d) * th.sum(w * th.exp(-1/(2* h**2) * th.pow(th.cdist(c, x), 2)).T, dim=1)
    #print(tmp.shape)
    return tmp
    
def _main():
    device = th.device('cpu')
    sigma_n = 25 / 255
    h = 0.04
    patch_size = 2
    kh, kw = patch_size, patch_size

    clean_I = th.mean(
        th.from_numpy(imageio.imread('./input.jpg') / 255), -1
    ).to(device)

    noisy_I = clean_I + clean_I.new_empty(clean_I.shape).normal_(0, sigma_n)

    noisy = util.image2patch(noisy_I.clone(), (kh, kw))

    #imageio.imwrite('./noisy_self.png',  noisy.numpy())

    estimate = noisy.clone()

    # dct_bank -> centers
    dct_bank, bank_weights = km.get_data(1000, patch_size, device=device)

    # We transform the "one-image" to patches and back to get the proper
    # averaging factor to combine the patches
    div = util.patch2image(
        util.image2patch(clean_I.new_ones(clean_I.shape), (kh, kw)),
        clean_I.shape,
        (kh, kw)
    )

    # We introduce a degree of freedom here since the optimal sigma depends
    # on the choice of h
    sigma = sigma_n * 1.5

    # Here we discard the first row since this corresponds to the mean
    D = dct.mat2d(patch_size)[1:].to(device)

    # prefactor = 1  # Calculate the prefactor (inverse mat in Eq. 21)
    prefactor = th.inverse(1.0/math.pow(h, 2) * D.T @ D + (1/math.pow(sigma, 2))*th.eye(D.shape[1]))

    # uncomment to show image while shifting
    # plt.ion()
    # f, a = plt.subplots(1, 3)
    # a[1].imshow(clean_I.cpu().numpy(), cmap='gray', vmin=0, vmax=1)
    # a[2].imshow(noisy_I.cpu().numpy(), cmap='gray', vmin=0, vmax=1)

    estimate_avg_list = []
    estimate_list = []
    pnsr_list = []

    printed_trajectories=[20,100,4000,50000,60000,123456]

    for i in range(10):
        estimate = mean_shift(
            estimate, noisy,
            dct_bank, bank_weights,
            sigma, h, D, prefactor
        )
        estimate_list.append([D @ estimate[traj].T for traj in printed_trajectories])
        
        # Transform to image and back to patches, essentially averaging
        # the intersection of the patches
        estimate_I = util.patch2image(estimate, clean_I.shape, (kh, kw)) / div
        estimate = util.image2patch(estimate_I, (kh, kw))
    
        print("Iteration", i+1, util.psnr(estimate_I, clean_I))    
        pnsr_list.append(util.psnr(estimate_I, clean_I))
        estimate_avg_list.append([D @ estimate[traj].T for traj in printed_trajectories])
        
        # uncomment to show image while shifting
        # a[0].imshow(estimate_I.cpu().numpy(), cmap='gray', vmin=0, vmax=1)
        # plt.pause(0.01)
        # plt.pause(1)

    estimate = util.patch2image(estimate, clean_I.shape, (kh, kw))
    imageio.imwrite('./out.png', estimate.clip_(0, 1).cpu().numpy())

    #uncomment to print trajectories
    vis.visualize_mean_shift(pnsr_list, estimate_list, h, len(printed_trajectories))
    vis.visualize_mean_shift(pnsr_list, estimate_avg_list, h, len(printed_trajectories))


if __name__ == '__main__':
    with th.no_grad():
        _main()
